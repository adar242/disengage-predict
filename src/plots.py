import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd


def plot_predictions(model_predictions):
    sns.set(style="whitegrid")

    data = pd.DataFrame()
    for model_name in model_predictions.keys():
        data[model_name] = model_predictions[model_name]

    g = sns.catplot(kind="bar", data=data, capsize=0.1)
    g.fig.set_size_inches(18, 8)

    plt.title("Models' Accuracy")
    plt.show()

def plot_feature_importance(model,relevant_features):
    sns.set(style="whitegrid")

    feature_importance = model.feature_importances_

    data = pd.DataFrame(columns=relevant_features)
    data.loc[0] = feature_importance

    g = sns.catplot(kind="bar", data=data)
    g.fig.set_size_inches(21, 8)
    plt.title("Feature importance")
    plt.show()

