import pandas as pd
import numpy as np

from sklearn.ensemble import GradientBoostingClassifier
from sklearn.ensemble import RandomForestRegressor
from sklearn.tree import DecisionTreeRegressor
from sklearn.svm import LinearSVC
from sklearn.linear_model import LogisticRegression

from sklearn.metrics import f1_score



citizen_data_path = '../Data/labled_citizen_data.csv'
n_iterations = 15

# PARAMS are dataframe X, target column y, and a float that represents what precent of the data should be taken for testing
# returns training X, testing X, training y and testing y based on given percent
def ordered_split(X, y, percent):
    dividing_index = int((len(y) / 100) * percent)

    training_X = X[:dividing_index]
    testing_X = X[dividing_index:]
    training_y = y[:dividing_index]
    testing_y = y[dividing_index:]
    return training_X, testing_X, training_y, testing_y


# drop given rows and update dataframe and csv
def drop_rows(citizen_data, row_indices):
    citizen_data.drop(row_indices, axis=0, inplace=True)
    citizen_data.to_csv(citizen_data_path, index=False)

# given the target column, return indices of rows not resolved (whose value is 0)
def clean_by_target(y):
    relevant_indices = []
    for index, value in enumerate(y):
        if value == 0:
            relevant_indices.append(index)

    return relevant_indices

# given a dictionary of models and the training data, fit each of the models to the data
def fit_models(models,training_X, training_y):
    for model in models.values():
        model.fit(training_X,training_y)

# given a dictionary of fitted models and testing data,
# return  a dictionary of model names and their predictions
def generate_predictions(models, testing_X):
    predictions = {}
    for model_name in models.keys():
        predictions[model_name] = models[model_name].predict(testing_X)
    return predictions


def print_model_scores(model_predictions, testing_y):

    arrayed_y = testing_y.to_numpy()
    models_accuracy = {}

    for model_name in model_predictions.keys():

        curr_model_predictions = model_predictions[model_name]
        curr_model_right_predicts = 0

        for i in range(len(arrayed_y)):
            if curr_model_predictions[i] == arrayed_y[i]:
                curr_model_right_predicts += 1


        models_accuracy[model_name] = curr_model_right_predicts / len(testing_y)

    # not printing model score as percent of right predictions
    for model_name in models_accuracy.keys():
        print(model_name, "percent of right predictions:", models_accuracy[model_name])

    return models_accuracy

def get_average_accuracy(testing_X, testing_y, training_X, training_y):
    models_average_accuracy = {"boosted tree model": [0] * n_iterations,
                               "decision tree model": [0] * n_iterations,
                               "linear svc model": [0] * n_iterations,
                               "logistic regression model": [0] * n_iterations,
                               "random model": [0] * n_iterations,
                               }
    for i in range(n_iterations):

        print("in iteration:", i)
        # specify models
        models = {
            "boosted tree model": GradientBoostingClassifier(),
            "decision tree model": DecisionTreeRegressor(),
            "linear svc model": LinearSVC(),
            "logistic regression model": LogisticRegression(),
        }

        # fit models
        fit_models(models, training_X, training_y)

        # generate predictions
        model_predictions = generate_predictions(models, testing_X)
        model_predictions["random model"] = np.random.choice([-1, 1], size=(len(testing_y),), p=[1 / 2, 1 / 2])

        # print score
        models_accuracy = print_model_scores(model_predictions, testing_y)

        for user_key in models_accuracy.keys():
            models_average_accuracy[user_key][i] = models_accuracy[user_key]

    return models_average_accuracy


def get_confusion_matrices(training_X, training_y, testing_X, testing_y):
    models_confusion_matrices = {"boosted tree model": np.zeros((2,2)),
                               "decision tree model": np.zeros((2,2)),
                               "linear svc model": np.zeros((2,2)),
                               "logistic regression model": np.zeros((2,2)),
                               "random model": np.zeros((2,2)),
                               }

    models = {
        "boosted tree model": GradientBoostingClassifier(),
        "decision tree model": DecisionTreeRegressor(),
        "linear svc model": LinearSVC(),
        "logistic regression model": LogisticRegression(),
    }

    # fit models
    fit_models(models, training_X, training_y)

    # generate predictions
    model_predictions = generate_predictions(models, testing_X)
    model_predictions["random model"] = np.random.choice([-1, 1], size=(len(testing_y),), p=[1 / 2, 1 / 2])

    # fill out the matrices
    for user_key in models_confusion_matrices.keys():

        for record_index in range(len(testing_y)):

            model_prediction = model_predictions[user_key][record_index]
            ground_truth = testing_y.iloc[record_index]

            # correct hit
            if model_prediction == 1 and ground_truth == 1:
                models_confusion_matrices[user_key][0][0] += 1
                continue

            # correct rejection
            if model_prediction == -1 and ground_truth == -1:
                models_confusion_matrices[user_key][1][1] += 1
                continue

            # miss
            if model_prediction == -1 and ground_truth == 1:
                models_confusion_matrices[user_key][0][1] += 1
                continue

            # false alarm
            if model_prediction == 1 and ground_truth == -1:
                models_confusion_matrices[user_key][1][0] += 1
                continue

    return models_confusion_matrices


def get_f1_score(training_X, testing_X, training_y, testing_y):
    models_average_score = {"boosted tree model": 0,
                               "decision tree model": 0,
                               "linear svc model": 0,
                               "logistic regression model": 0,
                               "random model": 0,
                               }

    for i in range(n_iterations):
        models = {
            "boosted tree model": GradientBoostingClassifier(),
            "decision tree model": DecisionTreeRegressor(),
            "linear svc model": LinearSVC(),
            "logistic regression model": LogisticRegression(),
        }

        # fit models
        fit_models(models, training_X, training_y)

        # generate predictions
        model_predictions = generate_predictions(models, testing_X)
        model_predictions["random model"] = np.random.choice([-1, 1], size=(len(testing_y),), p=[1 / 2, 1 / 2])

        for user_key in model_predictions.keys():
            models_average_score[user_key] += f1_score(testing_y, model_predictions[user_key])

    for user_key in model_predictions.keys():
        models_average_score[user_key] = models_average_score[user_key] / n_iterations

    return models_average_score

def main():

    # loading the data
    citizen_data = pd.read_csv(citizen_data_path)

    # create target
    y = citizen_data.disengaged.values.tolist()
    # create X
    relevant_features = ["hits_completed_average","current_hits_completed", "hits_completed_average_dif",
                         "dwell_time_average", "time_from_last_hit","current_dwell_time_average","dwell_time_average_dif",
                         "num_last_sessions", "session_time_average","current_session_time","session_time_average_dif"]

    X = citizen_data[relevant_features]

    training_X, testing_X, training_y, testing_y = ordered_split(X, y, 75)







if __name__ == "__main__":
    main()