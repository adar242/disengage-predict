import math

import pandas as pd
import numpy as np
from datetime import datetime
import json

citizen_data_path = '../Data/labled_citizen_data.csv'
user_sessions_path = '../Data/user_sessions.json'


# PARAMS  citizen data, band user sessions dictionary
# return an array of the disengaged label such that for each record:
#    if the record is less then 5 minutes away from the end of his respective session, disengage = +1
#    else, disengage = -1
def resolve_disengage_label(citizen_data, user_sessions):
    disengaged = [0] * len(citizen_data)

    data_end_time = citizen_data.tail(1)["timestamp"].array[0]
    data_end_time = datetime.strptime(data_end_time, "%m/%d/%y %H:%M")

    for user in user_sessions.keys():

        for session in user_sessions[user]:

            for (hit_timestamp, df_index) in session:
                t1 = datetime.strptime(hit_timestamp,"%m/%d/%y %H:%M")
                last_hit_timestamp = session[-1][0]
                t2 = datetime.strptime(last_hit_timestamp,"%m/%d/%y %H:%M")

                # less then 30 minutes from end of data, cant be sure if disengaged
                if (data_end_time - t1).total_seconds() < 30 * 60:
                    disengaged[df_index] = 0
                    continue

                if (t2 - t1).total_seconds() < 5 * 60:
                    disengaged[df_index] = 1
                else:
                    disengaged[df_index] = -1

    return disengaged

# PARAM dictionary of user sessions
# resolve the average amount of hits per session per user
# return a dictionary of the averaged hits per session of each user
def hits_completed_average(user_sessions):
    user_completed_averages = {}

    for user in user_sessions.keys():
        hits_sum = 0

        for session in user_sessions[user]:
            hits_sum += len(session)

        hits_avg = hits_sum / len(user_sessions[user])
        user_completed_averages[user] = hits_avg

    return user_completed_averages

# PARAM dictionary of user sessions and the citizen data labled data frame
# resolve the current amount of hits in session for each hit
# return an array the size of citizen_user with current hit ammount in session for each hit.
def current_hits_completed(user_sessions, citizen_data):
    current_hits_completed = []

    # utility dict to keep save amount of indexes in relevant session
    user_current_session_size = {}

    for index, row in citizen_data.iterrows():

        if row.user_key not in user_current_session_size.keys():
            user_current_session_size[row.user_key] = 0
        if index % 50 == 0:
            print("in iteration: ", index)
        # find the hit
        for session in user_sessions[row.user_key]:

            for hit_timestamp in session:
                hit_timestamp = hit_timestamp[0] # get just the timestamp
                if hit_timestamp == row.timestamp:  # this is the relevant hit
                    user_current_session_size[row.user_key] += 1
                    current_hits_completed.append(user_current_session_size[row.user_key])
                    if user_current_session_size[row.user_key] == len(session): # this is the last hit in the session
                        user_current_session_size[row.user_key] = 0
                    break

    return current_hits_completed

# PARAM citizen data panda dataframe, with hits_completed_average and current_hits_completed features
# returns an array of the difference between the hits completed and the average hits for each record
def hits_completed_average_dif(citizen_data):
    hits_completed_average_dif = []

    for index, row in citizen_data.iterrows():
        if index % 50 == 0:
            print("in iteration: ", index)

        average_dif = row.hits_completed_average - row.current_hits_completed
        hits_completed_average_dif.append(average_dif)

    return hits_completed_average_dif

# PARAMS user session dictionary
# returns a dictionary such that the keys are user names, and the values are the average time between hits
# in same session
def dwell_time_average(user_sessions):
    user_dwell_average = {}

    for user in user_sessions.keys():
        dwell_sum = 0

        for session in user_sessions[user]:
            session_dwell_sum = 0

            for hit_index in range(len(session)):
                if hit_index + 1 == len(session):
                    continue

                t1 = datetime.strptime(session[hit_index][0], "%m/%d/%y %H:%M")
                t2 = datetime.strptime(session[hit_index + 1][0], "%m/%d/%y %H:%M")

                session_dwell_sum += (t2 - t1).total_seconds()

            if len(session) == 1:
                dwell_sum += 0
                continue

            dwell_sum += session_dwell_sum / (len(session) - 1)

        dwell_average = dwell_sum / len(user_sessions[user])

        user_dwell_average[user] = dwell_average

    return user_dwell_average



# PARAM citizen data dataframe with time_from_last_hit feature, user sessions dictionary
# returns an array size of citizen_data with the durrent dwell time running average for the user
def current_dwell_time_average(citizen_data, user_sessions):
    current_dwell_time_average = [0] * len(citizen_data)

    for user in user_sessions.keys():

        for session in user_sessions[user]:
            session_dwell_sum = 0

            for hit_index, (hit_timestamp, df_index) in enumerate(session):

                if hit_index == 0: # first hit of session, so current average is 0
                    current_dwell_time_average[df_index] = 0
                else: # not the first hit of session
                    t1 = datetime.strptime(session[hit_index - 1][0], "%m/%d/%y %H:%M")
                    t2 = datetime.strptime(session[hit_index][0], "%m/%d/%y %H:%M")

                    session_dwell_sum += (t2 - t1).total_seconds()

                    current_dwell_time_average[df_index] = session_dwell_sum / hit_index

    return current_dwell_time_average


# PARAM citizen dataframe with average dwell time and current avarage dwell time features
# return an array corresponding to citizen data with the difference between the features
def dwell_time_average_dif(citizen_data):
    dwell_time_average_dif = []

    for index, row in citizen_data.iterrows():
        if index % 50 == 0:
            print("in iteration: ", index)

        average_dif = row.dwell_time_average - row.current_dwell_time_average
        dwell_time_average_dif.append(average_dif)

    return dwell_time_average_dif

# PARAMS user sessions dictionary and citizen data dataframe
# returns an array corresponding to citizen science with the time spent from previous hit in the same session.
# first hit in session will be 0
def time_from_last_hit(citizen_data, user_sessions):
    time_from_last_hit = [0] * len(citizen_data)

    for user_key in user_sessions.keys():
        for session in user_sessions[user_key]:
            for session_index, (hit_timestamp, df_index) in enumerate(session):
                # if first hit of session
                if session_index == 0:
                    time_from_last_hit[df_index] = 0
                else: # not first hit of session
                    t1 = datetime.strptime(session[session_index - 1][0], "%m/%d/%y %H:%M")
                    t2 = datetime.strptime(hit_timestamp,  "%m/%d/%y %H:%M")
                    time_from_last_hit[df_index] = (t2 - t1).total_seconds()

    return time_from_last_hit

# PARAMS user sessions dictionary and citizen data dataframe
# returns an array corresponding to citizen science with the amount of last sessions in data frame
def num_last_sessions(citizen_data, user_sessions):
    num_last_sessions = [0] * len(citizen_data)

    for user_key in user_sessions.keys():
        for session_index, session in enumerate(user_sessions[user_key]):
            for (hit_timestamp, df_index) in session:

                if session_index != 0: # this is not the first session, so num_last_sessions >= 1
                    num_last_sessions[df_index] = session_index

    return num_last_sessions

# PARAMS user sessions dictionary
# returns a dictionary with the average time spent on a session for each user
def session_time_average(user_sessions):
    user_session_average = {}

    for user in user_sessions.keys():

        sessions_sum = 0

        for session in user_sessions[user]:
            t1 = datetime.strptime(session[0][0], "%m/%d/%y %H:%M")  # first hit of session
            t2 = datetime.strptime(session[-1][0], "%m/%d/%y %H:%M")  # last hit of session

            time_of_session = (t2 - t1).total_seconds()
            sessions_sum += time_of_session

        user_session_average[user] = sessions_sum / len(user_sessions[user])
    return user_session_average


# PARAMS user sessions dictionary and citizen data dataframe
# returns an array corresponding to citizen data with the time from start of session for each hit
def current_session_time(user_sessions, citizen_data):
    curr_sess_time = [0] * len(citizen_data)

    for user in user_sessions.keys():
        for session in user_sessions[user]:
            for (hit_timestamp, df_index) in session:

                t1 = datetime.strptime(session[0][0], "%m/%d/%y %H:%M")  # first hit of session
                t2 = datetime.strptime(hit_timestamp, "%m/%d/%y %H:%M")  # current hit of  session

                time_since_start = (t2 - t1).total_seconds()

                curr_sess_time[df_index] = time_since_start

    return curr_sess_time


# PARAM citizen dataframe with session_time_average and current session time features
# return an array corresponding to citizen data with the difference between the features
def sess_time_average_dif(citizen_data):
    sess_time_average_dif = []

    for index, row in citizen_data.iterrows():
        if index % 50 == 0:
            print("in iteration: ", index)

        average_dif = row.session_time_average - row.current_session_time
        sess_time_average_dif.append(average_dif)

    return sess_time_average_dif

# PARAM panda dataframe
# returns a dictionary such that the keys are user keys, and the values are arrays of arrays, containing tuple of HIT
# and the index in the table grouped by their sessions.
def get_users_sessions(citizen_data):
    user_sessions = {}

    for index, row in citizen_data.iterrows():
        if index % 50 == 0:
            print("in iteration ", index)

        t1 = datetime.strptime(row.timestamp, "%m/%d/%y %H:%M")

        if row.user_key not in user_sessions.keys():
            # first entry for this user
            user_sessions[row.user_key] = [[]]
            user_sessions[row.user_key][0].append((row.timestamp, index))

            # search for other entries for this user
            for compared_index, compared_row in citizen_data.iloc[index:, :].iterrows():
                if index == compared_index:
                    continue

                if compared_row.user_key != row.user_key:  # not same user
                    continue
                else: # same user
                    t2 = datetime.strptime(compared_row.timestamp, "%m/%d/%y %H:%M")

                    # compared hit is in same session
                    if (t2 - t1).total_seconds() < 30 * 60:
                        user_sessions[row.user_key][-1].append((compared_row.timestamp, compared_index))
                        t1 = t2

                    # compared row is in another session
                    else:
                        user_sessions[row.user_key].append([])
                        user_sessions[row.user_key][-1].append((compared_row.timestamp, compared_index))
                        t1 = t2
    return user_sessions


# updates the data in csv with the new array
# returns updated panda table
def update_table(citizen_data, array, name):
    citizen_data[name] = array
    citizen_data.to_csv(citizen_data_path, index=False)
    return citizen_data


# PARAMS a dict which keys are user keys.
# update the dataframe and the csv table with the dict values for each user
def update_table_by_dict(user_dict,citizen_data, name):
    column = []

    for index, row in citizen_data.iterrows():
        value = user_dict[row.user_key]
        column.append(value)

    citizen_data[name] = column
    citizen_data.to_csv(citizen_data_path, index=False)


def check_sorted(citizen_data):
    result = True
    for index, row in citizen_data.iterrows():
        if index == 0:
            continue

        t2 = row.timestamp
        x = citizen_data.iloc[index - 1]
        t1 = x.timestamp

        if t2 < t1:
            print("FALSE")
            print(t2)
            print(t1)
            result = False
    print("True")
    return result


if __name__ == "__main__":
    pass















